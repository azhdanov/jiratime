# Jira Time

Load Jira Time Tracking data over REST

# Usage

Start server

    npm install
    node start https://exampleorg.atlassian.net

Client

    curl -u "user@example.org:api-token" \
        https://localhost:3000?start=2017-08-21\&end=2017-08-31\&user=admin\&groups=administrators


Supported parameters:

* start - required start date in YYYY-MM-DD format
* end - required end date in YYYY-MM-DD format
* user - optional user key,
* groups - optional coma separated group names
* projectid - optional, e.g. 10001 (not project key!),
* moreFields - optional coma separated list of fields to include in issue details

Example response

    [
      {
        "expand": "operations,versionedRepresentations,editmeta,changelog,renderedFields",
        "id": "10000",
        "self": "https://primetimesheet.atlassian.net/rest/api/2/issue/10000",
        "key": "DEMO-1",
        "fields": {
          "summary": "As an Agile team, I'd like to learn about Scrum >> Click the \"DEMO-1\" link at the left of this row to see detail in the Description tab on the right",
          "issuetype": {
            "self": "https://primetimesheet.atlassian.net/rest/api/2/issuetype/10000",
            "id": "10000",
            "description": "Created by Jira Agile - do not edit or delete. Issue type for a user story.",
            "iconUrl": "https://primetimesheet.atlassian.net/images/icons/issuetypes/story.svg",
            "name": "Story",
            "subtask": false
          },
          "customfield_10007": null,
          "project": {
            "self": "https://primetimesheet.atlassian.net/rest/api/2/project/10000",
            "id": "10000",
            "key": "DEMO",
            "name": "Demonstration Project",
            "avatarUrls": {
              "48x48": "https://primetimesheet.atlassian.net/secure/projectavatar?avatarId=10324",
              "24x24": "https://primetimesheet.atlassian.net/secure/projectavatar?size=small&avatarId=10324",
              "16x16": "https://primetimesheet.atlassian.net/secure/projectavatar?size=xsmall&avatarId=10324",
              "32x32": "https://primetimesheet.atlassian.net/secure/projectavatar?size=medium&avatarId=10324"
            },
            "projectCategory": {
              "self": "https://primetimesheet.atlassian.net/rest/api/2/projectCategory/10000",
              "id": "10000",
              "description": "",
              "name": "Test Category"
            }
          },
          "issuelinks": [],
          "priority": {
            "self": "https://primetimesheet.atlassian.net/rest/api/2/priority/3",
            "iconUrl": "https://primetimesheet.atlassian.net/images/icons/priorities/medium.svg",
            "name": "Medium",
            "id": "3"
          },
          "resolution": null,
          "status": {
            "self": "https://primetimesheet.atlassian.net/rest/api/2/status/10000",
            "description": "",
            "iconUrl": "https://primetimesheet.atlassian.net/",
            "name": "To Do",
            "id": "10000",
            "statusCategory": {
              "self": "https://primetimesheet.atlassian.net/rest/api/2/statuscategory/2",
              "id": 2,
              "key": "new",
              "colorName": "blue-gray",
              "name": "To Do"
            }
          },
          "worklog": {
            "worklogs": [
              {
                "author": "admin",
                "comment": "test",
                "created": "2017-08-21T22:02:27.140+0200",
                "updated": "2017-08-21T22:02:27.140+0200",
                "visibility": {
                  "type": "group",
                  "value": "administrators"
                },
                "started": "2017-08-21T20:02:00.000Z",
                "timeSpent": "1h",
                "timeSpentSeconds": 3600,
                "id": "21609",
                "issueId": "10000"
              }
            ]
          }
        }
      }
    ]

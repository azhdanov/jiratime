const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const worklog = require('./worklog');

if (process.argv.length != 3) {
  console.log("USAGE: node server.js https://exampleorg.atlassian.net");
  return;
}

const server = http.createServer(worklog);

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

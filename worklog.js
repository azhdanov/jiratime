var request = require('request');
var url = require('url');
var MAX_BULK_LENGTH = 5;
var onAllIssuesPromises = [];
var issueWorklogPromises = [];

module.exports = function (req, res) {

    var authRequest = function (method, options) {

        return new Promise(function(resolve, reject) {
            options.headers = {
                'Authorization': req.headers.authorization
            };
            options.url = process.argv[2] + options.url;
            method(options, function(err, res, body) {
                if (!err && res.statusCode == 200) {
                    var data = getJson(body);
                    resolve(data);
                } else {
                    //console.log('request', options.uri || options, err || res.statusCode, body);
                    reject(err || body);
                }
            });
        });
    };

    var get = function (url) {
        return authRequest(request.get.bind(request), {url: url});
    };

    var getIssues = function (query) {
        return authRequest(request.post.bind(request), {
            url: '/rest/api/2/search',
            json: query
        });
    };

    var onAllIssues = function (query, callback) {
        query.startAt = 0;
        var firstDeferred = getIssues( query);
        var promise = firstDeferred.then(callback);
        var promises = [promise];
        onAllIssuesPromises.push(promise);
        return promise.then(function (data) {
            for (var i = 1; i < Math.ceil(data.total / data.maxResults); i++) {
                query.startAt = data.maxResults * i
                var previous = onAllIssuesPromises.length > MAX_BULK_LENGTH ? onAllIssuesPromises[onAllIssuesPromises.length - MAX_BULK_LENGTH] : Promise.resolve(); // promises[1 .. ]
                var next = previous.then((function (query) { // closure in loop
                    return function () {
                        return getIssues(query).then(callback);
                    };
                })(Object.assign({}, query)));
                promises.push(next);
                onAllIssuesPromises.push(next);
            }
            return Promise.all(promises);
        });
    };

    var getUserInfo = function (username) {
        return get('/rest/api/2/user?expand=groups&key=' + encodeURIComponent(username));
    };

    var getFields = function () {
        return get('/rest/api/2/field');
    };

    var quote = function(value) {
        return '"' + value + '"';
    };

    var jqlEscape = function(value) {
        return value
            .replace('"', '\\u0022')
            .replace('#', '\\u0023')
            .replace('$', '\\u0024')
            .replace('%', '\\u0025')
            .replace('*', '\\u002a')
            .replace('@', '\\u0040')
            .replace('^', '\\u005e');
        // <>& unsupported by user management
    };

    var filterIssueWorklogs = function(issue, groups) {
        var promises = [];
        var issueWorklogs = issue.fields.worklog.worklogs;
        if (issueWorklogs && issueWorklogs.length > 0) {
            var worklogCopy = issueWorklogs.splice(0, issueWorklogs.length); // copy & reset
            worklogCopy.forEach(function (worklog) {
                var username = worklog.author.key || worklog.author.name;
                if (!req.userGroups.hasOwnProperty(username)) {
                    req.userGroups[username] = getUserInfo(username);
                    promises.push(req.userGroups[username]);
                }
                req.userGroups[username].then(function (user) {
                    for (var i = 0; i < user.groups.items.length; i++) {
                        if (groups.indexOf(user.groups.items[i].name) >= 0) {
                            issueWorklogs.push(worklog);
                            break;
                        }
                    }
                });
            });
        }
        return Promise.all(promises).then(function () {
            return issue;
        });
    };

    var getIssueWorklog = function (issueKey /* for cache , issueId*/) {
        var promises = issueWorklogPromises;
        var previous = promises.length < MAX_BULK_LENGTH ? Promise.resolve() : promises[promises.length - MAX_BULK_LENGTH]; // promises[0 .. ]
        return previous.then(function () {
            var promise = get('/rest/api/2/issue/' + issueKey + '/worklog');
            promises.push(promise);
            return promise;
        });
    };

    var loadAllWorklogs = function (issue) {
        var worklog = issue.fields.worklog;
        if (!worklog) {
            worklog = issue.fields.worklog = {total: 1, maxResults: 0, worklogs: []}; // force load worklog
        }
        if (worklog.total > worklog.maxResults) {
            return getIssueWorklog(issue.key, issue.id).then(function (worklog) {
                issue.fields.worklog = worklog;
                return issue;
            });
        } else {
            return Promise.resolve(issue);
        }
    };

    var getJiraWorklog = function (req, res) {
        var parts = url.parse(req.url, true);
        req.query = parts.query;
        var $fields = getFields();
        Promise.all([$fields]).then(function (data) {
            var allFields = data[0];
            var jqlQueryBuilder = req.query.sumSubTasks ? new queryBuilder(" and ") : getSearchQueryBuilder(req.query);
            console.log('start/end', req.query.start, req.query.start);
            var start = req.query.start ? new Date(req.query.start) : new Date(); // TODO: -1
            var end = req.query.end ? new Date(req.query.end) : new Date(); // TODO: +1
            console.log('start/end', start, end);
            jqlQueryBuilder.addQuery('worklogDate', quote(formatDateForSearch(start)), '>=');
            jqlQueryBuilder.addQuery('worklogDate', quote(formatDateForSearch(end)), '<=');
            if (req.query.user) {
                jqlQueryBuilder.addQuery('worklogAuthor', jqlEscape(req.query.user));
            }
            if (req.query.groups) {
                req.userGroups = {}; // TODO: cache
                if (!Array.isArray(req.query.groups)) {
                    req.query.groups = req.query.groups.split(',');
                }
                var memberOfQB = new queryBuilder(" or ");
                for (var i = 0; i < req.query.groups.length; i++) {
                    memberOfQB.addQueryWithFunc('worklogAuthor', quote(jqlEscape(req.query.groups[i])), ' in ', "membersOf");
                }
                jqlQueryBuilder.addQueryBuilder(memberOfQB);
            }
            var jqlQuery = jqlQueryBuilder.build();
            console.log('jqlQuery', jqlQuery);
            var fields = 'project,issuetype,resolution,summary,priority,status,parent,issuelinks';
            var epicLinkField  = getEpicLinkField(allFields);
            if (epicLinkField) {
                fields += ',' + epicLinkField.id;
            }
            if (req.query.moreFields) {
                fields += ',' + (Array.isArray(req.query.moreFields) ? req.query.moreFields.join(',') : req.query.moreFields);
            }
            if (req.query.sumSubTasks) {
                fields += ',subtasks';
            }
            var issuesQuery = {
                jql: jqlQuery,
                fields: fields.split(','),
                maxResults: 1000
            };
            onAllIssues(issuesQuery, function (data) {
                var promises = data.issues.map(function (issue) {
                    return loadAllWorklogs(issue).then(function (issue) {
                        var worklog = issue.fields.worklog;
                        worklog.worklogs = worklog.worklogs.filter(function (worklog) {
                            var started = new Date(worklog.started);
                            return started >= start && started < end;
                        });
                        if (req.query.groups) {
                            return filterIssueWorklogs(issue, req.query.groups);
                        } else {
                            return issue;
                        }
                    });
                });
                return Promise.all(promises);
            }).then(function (issues) {
                var allIssues = issues.reduce(function (a, b) { // flatten
                    return a.concat(b);
                }, []);
                res.end(JSON.stringify(allIssues));
            }).catch(function (err) {
                console.log('issues rejected', err);
                res.statusCode = 500
                res.end(err);
            });
        }).catch(function (err) {
            console.log('fields rejected', err);
            res.statusCode = 500
            res.end(err.toString());
        });
    };

    getJiraWorklog(req, res);

    var getEpicLinkField = function (allFields) {
        for (var i = 0; i < allFields.length; i++) {
            var field = allFields[i];
            if (field.custom && field.schema != null && field.schema.custom == "com.pyxis.greenhopper.jira:gh-epic-link") {
                return field;
            }
        }
    };

    var getSearchQueryBuilder = function(options) {

        var getFilterOrProjectValue = function(options, startString) {
            var result = null;
            if (options.filterOrProjectId) {
                var items = Array.isArray(options.filterOrProjectId)
                    ? options.filterOrProjectId
                    : options.filterOrProjectId.split(',');
                result = [];
                for (var i = 0; i < items.length; i++) {
                    if (items[i].indexOf(startString) == 0) {
                        result.push(items[i].split(startString)[1]);
                    }
                }
            }
            return result;
        };
        var getOrQueryBuilder = function(paramName, items) {
            var qb = new queryBuilder(" or ");
            for (var i = 0; i < items.length; i++) {
                qb.addQuery(paramName, '"' + items[i] + '"');
            }
            return qb;
        };
        var filterIds = getFilterOrProjectValue(options, 'filter_');
        var projectKeys = getFilterOrProjectValue(options, 'project_');
        var hasJQL = options.jql != null; // empty JQL for all issues
        if (!filterIds && !projectKeys && !hasJQL && options.projectKey) {
            projectKeys = [options.projectKey];
        }

        // performance optimization hack, see $allIssueIds
        options.projectKeys = projectKeys;
        options.filterIds = filterIds;

        var jqlQueryBuilder = new queryBuilder(" and ");
        if (hasJQL) {
            jqlQueryBuilder.addStatement(options.jql);
        }
        if (filterIds && filterIds.length > 0) {
            // https://answers.atlassian.com/questions/22264/can-filters-be-used-with-the-rest-api
            if (filterIds.length == 1) {
                jqlQueryBuilder.addQuery('filter', filterIds[0]);
            } else {
                jqlQueryBuilder.addQueryBuilder(getOrQueryBuilder('filter', filterIds));
            }
        }
        if (projectKeys && projectKeys.length > 0) {
            // https://answers.atlassian.com/questions/155180/filter-issue-by-project-key
            if (projectKeys.length == 1) {
                jqlQueryBuilder.addQuery('project', '"' + projectKeys[0] + '"');
            } else {
                jqlQueryBuilder.addQueryBuilder(getOrQueryBuilder('project', projectKeys));
            }
        }
        return jqlQueryBuilder;
    };

    var queryBuilder = function(delimiter) {
        var items = [];
        delimiter = delimiter || "&";

        this.addStatement = function(statement) {
            items.push(statement);
            return this;
        };

        this.addQuery = function(param, value, delimiter) {
            delimiter = delimiter || "=";
            if (value != null && value != "") {
                this.addStatement(param + delimiter + value);
            }
            return this;
        };

        this.addQueryWithFunc = function(param, value, delimiter, queryFunc) {
            this.addQuery(param, queryFunc + "(" + value + ")", delimiter);
        };

        this.addQueryBuilder = function(queryBuilder) {
            this.addStatement("(" + queryBuilder.build() + ")");
        };

        this.build = function() {
            return this.isEmpty()
                ? ""
                : items.join(delimiter);
        };

        this.isEmpty = function() {
            return items == null || items.length == 0;
        };
    };

    var normalizeNumber = function (number){
        return (number < 10 ? '0' : '') + number;
    };

    var formatDateForSearch = function (date) {
        return date.getFullYear() + "-" + normalizeNumber(date.getMonth() + 1) + "-" + normalizeNumber(date.getDate());
    };

    var getJson = function (data, defaultValue) {
        if (typeof data == 'string') {
            try {
                data = data != "" ? JSON.parse(data) : {};
            } catch (e) {
                console.log(e, data);
                data = defaultValue || {};
            }
        }
        return data;
    };
};
